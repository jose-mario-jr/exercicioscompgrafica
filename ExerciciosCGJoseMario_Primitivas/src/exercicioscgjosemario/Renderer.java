package exercicioscgjosemario;

import java.awt.Color;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import java.awt.List;
import java.util.ArrayList;
import javax.swing.JFrame;

public class Renderer implements GLEventListener {

    @Override
    public void display(GLAutoDrawable drawable) {

        GL2 gl = drawable.getGL().getGL2();

        gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

        //Ex1(gl);
        //Ex2(gl);
        //Ex3(gl);
        //Ex4(gl);
        //DispOriginal(gl);
        //Ex4Brincando(gl);
        
        gl.glFlush();

    }

//    public static int factorInv(int factor) {
//        if (factor == 1) {
//            return 2;
//        } else {
//            return 1;
//        }
//    }
    public static void Ex1(GL2 gl) {
        //ex 1 - pontos
        gl.glColor3f(1.0f, 1.0f, 1.0f);
        float pos = -0.9f;
        for (int i = 1; pos < 0.95; i++) {
            gl.glPointSize((float) i / 2.0f);
            gl.glBegin(GL.GL_POINTS);
            gl.glVertex2f(pos, 0.0f);
            pos += 0.1;
            gl.glEnd();
        }
    }

    public static void Ex2(GL2 gl) {
        //ex 2 - linhas

        gl.glLineWidth(5f);
        gl.glEnable(GL2.GL_LINE_STIPPLE);
        gl.glLineStipple(1, (short) 0xF0F0);

        float pos = -0.9f;
        int factor = 1;
        ArrayList<Integer> pat = new ArrayList<>();
        pat.add(0x00FF);
        pat.add(0x00FF);
        pat.add(0x0C0C);
        pat.add(0x0C0C);
        pat.add(0x0C0F);
        pat.add(0x0C0F);
        pat.add(0xAAAA);
        pat.add(0xAAAA);
        pat.add(0xAAFF);
        pat.add(0xAAFF);
        pat.add(0xFF00);
        pat.add(0xFF00);
        pat.add(0xFFCC);
        pat.add(0xFFCC);
        pat.add(0xCC00);
        pat.add(0xCC00);
        int i;
        for (i = 1; i < 7; i++) {
            gl.glLineStipple(factor, pat.get(i).shortValue());
            gl.glBegin(GL.GL_LINES);
            gl.glVertex2f(pos, 0.9f); //inicio
            gl.glVertex2f(0.9f, pos); // fim
            gl.glEnd();
            pos = pos + .1f;

            factor = factor == 1 ? 2 : 1;

        }
        System.out.println(i);
        pos = 0.9f;
        for (i = 7; i < 14; i++) {
            gl.glLineStipple(factor, pat.get(i).shortValue());
            gl.glBegin(GL.GL_LINES);
            gl.glVertex2f(-.9f, pos); //inicio
            gl.glVertex2f(pos, -.9f); // fim
            gl.glEnd();
            pos = pos - .1f;
            factor = factor == 1 ? 2 : 1;
        }

        gl.glDisable(GL2.GL_LINE_STIPPLE);

//                System.out.println((short) 0x0000);
//                System.out.println((short) 0xFFFF);
//                System.out.println((short) 0x00FF);
//                System.out.println((short) 0x0C0F);
//                System.out.println((short) 0xAAAA);
//                System.out.println((short) 0xF0F0);
    }

    public static void Ex3(GL2 gl) {
        // exercicio 3: 
        gl.glColor3f(1.0f, 1.0f, 1.0f);

        gl.glBegin(GL2.GL_QUADS);
        gl.glVertex2f(0.75f, 0.75f);
        gl.glVertex2f(0.75f, -0.75f);
        gl.glVertex2f(-0.75f, -0.75f);
        gl.glVertex2f(-0.75f, 0.75f);

        gl.glEnd();

        gl.glColor3f(1.0f, 0.0f, 0.0f);

        gl.glBegin(GL.GL_TRIANGLES);
        gl.glVertex2f(0.0f, 0.25f);
        gl.glVertex2f(-0.25f, -0.25f);
        gl.glVertex2f(0.25f, -0.25f);
        gl.glEnd();
    }

    public static void Ex4(GL2 gl) {
        // exercicio 4 - casinha:

        //porta
        gl.glBegin(GL2.GL_QUADS);
        gl.glVertex2f(-0.5f, -0.20f);
        gl.glVertex2f(-0.25f, -0.20f);
        gl.glVertex2f(-0.25f, -0.75f);
        gl.glVertex2f(-0.5f, -0.75f);
        gl.glEnd();

        //|_| de frente à porta
        gl.glBegin(GL.GL_LINE_STRIP);
        gl.glVertex2f(-0.75f, -0.0f);
        gl.glVertex2f(-0.75f, -0.75f);
        gl.glVertex2f(-0.0f, -0.75f);
        gl.glVertex2f(0.0f, 0.0f);
        gl.glEnd();

        //resto da parte fina da casa
        gl.glBegin(GL.GL_LINE_STRIP);
        gl.glVertex2f(-0.0f, -0.75f);
        gl.glVertex2f(0.75f, -0.5f);
        gl.glVertex2f(0.75f, 0.25f);
        gl.glEnd();

        //triangulo frente da casa
        gl.glLineWidth(3f);
        gl.glBegin(GL.GL_LINE_LOOP);
        gl.glVertex2f(-0.8f, -0.0f);
        gl.glVertex2f(-0.375f, 0.375f);
        gl.glVertex2f(0.05f, 0.0f);
        gl.glEnd();

        //telhado
        gl.glBegin(GL2.GL_QUADS);
        gl.glVertex2f(-0.375f, 0.375f);
        gl.glVertex2f(0.375f, 0.625f);
        gl.glVertex2f(0.8f, 0.25f);
        gl.glVertex2f(0.05f, 0.0f);
        gl.glEnd();

        //Janela
        gl.glBegin(GL2.GL_QUADS);
        gl.glVertex2f(0.225f, -0.425f);
        gl.glVertex2f(0.6f, -0.3f);
        gl.glVertex2f(0.6f, 0.0f);
        gl.glVertex2f(0.225f, -0.125f);
        gl.glEnd();
    }


    public static void DispOriginal(GL2 gl) {
        gl.glColor3f(1.0f, 1.0f, 1.0f);
        gl.glRectf(-0.75f, -0.25f, -0.25f, 0.25f);

        gl.glEnable(GL2.GL_POLYGON_STIPPLE);
        gl.glPolygonStipple(getMaskFly(), 0);
        gl.glRectf(-0.25f, -0.25f, 0.25f, 0.25f);

        gl.glPolygonStipple(getMaskHalftone(), 0);
        gl.glRectf(0.25f, -0.25f, 0.75f, 0.25f);
        gl.glDisable(GL2.GL_POLYGON_STIPPLE);

    }

    public static void Ex4Brincando(GL2 gl) {

        //porta
        gl.glBegin(GL2.GL_QUADS);
        gl.glVertex2f(-0.5f, -0.20f);
        gl.glVertex2f(-0.25f, -0.20f);
        gl.glVertex2f(-0.25f, -0.75f);
        gl.glVertex2f(-0.5f, -0.75f);
        gl.glEnd();

        //|_| de frente à porta
        gl.glBegin(GL.GL_LINE_STRIP);
        gl.glVertex2f(-0.75f, -0.0f);
        gl.glVertex2f(-0.75f, -0.75f);
        gl.glVertex2f(-0.0f, -0.75f);
        gl.glVertex2f(0.0f, 0.0f);
        gl.glEnd();

        //resto da parte fina da casa
        gl.glBegin(GL.GL_LINE_STRIP);
        gl.glVertex2f(-0.0f, -0.75f);
        gl.glVertex2f(0.75f, -0.5f);
        gl.glVertex2f(0.75f, 0.25f);
        gl.glEnd();

        //triangulo frente da casa
        gl.glLineWidth(3f);
        gl.glBegin(GL.GL_LINE_LOOP);
        gl.glVertex2f(-0.8f, -0.0f);
        gl.glVertex2f(-0.375f, 0.375f);
        gl.glVertex2f(0.05f, 0.0f);
        gl.glEnd();

        //telhado
        gl.glBegin(GL2.GL_QUADS);
        gl.glVertex2f(-0.375f, 0.375f);
        gl.glVertex2f(0.375f, 0.625f);
        gl.glVertex2f(0.8f, 0.25f);
        gl.glVertex2f(0.05f, 0.0f);
        gl.glEnd();

        //Janela
        gl.glBegin(GL2.GL_QUADS);
        gl.glVertex2f(0.225f, -0.425f);
        gl.glVertex2f(0.6f, -0.3f);
        gl.glVertex2f(0.6f, 0.0f);
        gl.glVertex2f(0.225f, -0.125f);
        gl.glEnd();

        //fundo
        gl.glEnable(GL2.GL_POLYGON_STIPPLE);

        gl.glPolygonStipple(getMaskHalftone(), 0);
        gl.glRectf(-1.0f, -1.0f, 1f, 1.0f);
        gl.glDisable(GL2.GL_POLYGON_STIPPLE);

    }

	void dda(int x1, int y1, int x2, int y2, GL2 gl) {
        //gl.glPointSize(2.0f);
        gl.glBegin(GL2.GL_POINTS);
        gl.glColor3f(1.0f, 1.0f, 1.0f);

        int step;
        float x, y, xInc, yInc;
        step = Math.abs(x2 - x1);
        if (Math.abs(y2 - y1) > step) {
            step = Math.abs(y2 - y1);
        }
        xInc = (float) (x2 - x1) / step;
        yInc = (float) (y2 - y1) / step;
        x = x1;
        y = y1;
        while (x < x2) {
            gl.glVertex2i(Math.round(x), Math.round(y));
            x += xInc;
            y += yInc;
        }
        gl.glEnd();
    }

    void bresenhamCompleto(int x1, int y1, int x2, int y2, GL2 gl) {
        int dx, dy, d, x, y;
        int inclinacao = 0;
        dx = x2 - x1;
        dy = y2 - y1;
        if (dx < 0) {
            bresenhamCompleto(x2, y2, x1, y1, gl);
            return;
        }
        if (dy < 0) {
            inclinacao = -1;
        } else {
            inclinacao = 1;
        }
        x = x1;
        y = y1;
        gl.glVertex2i(x, y);
        if (dx >= inclinacao * dy) { //m<=1
            if (dy <= 0) {//caso y2 < y1
                d = 2 * (dy + dx);
                while (x < x2) {
                    if (d < 0) { // escolhido é o ponto de baixo
                        d += 2 * (dy + dx);
                        x++;
                        y--;
                    } else { // escolhido é o ponto de cima
                        d += 2 * dy;
                        x++;
                    }
                    gl.glVertex2i(x, y);
                }
            } else {//y1 < y2
                d = 2 * (dy - dx);
                while (x < x2) {
                    if (d <= 0) {// escolhido é o ponto de baixo
                        d += (2 * dy);
                        x++;
                    } else {// escolhido é o ponto de cima
                        d += 2 * (dy - dx);
                        x++;
                        y++;
                    }
                    gl.glVertex2i(x, y);
                }
            }
        } else {//|m| > 1
            if (dy < 0) { // caso y2<y1
                d = dy + (2 * dx);
                while (y > y2) {
                    if (d <= 0) {
                        d += 2 * dx;
                        y--; // varia apenas no eixo y
                    } else {
                        d += 2 * (dy + dx);
                        x++;
                        y--;
                    }
                    gl.glVertex2i(x, y);
                }
            } else { // caso y1<y2
                d = dy - (2 * dx);
                while (y < y2) {
                    if (d <= 0) {
                        d += 2 * (dy - dx);
                        x++;
                        y++;
                    } else {
                        d += -2 * dx;
                        y++; // varia apenas no eixo y
                    }
                    gl.glVertex2i(x, y);
                }
            }
        }
        gl.glVertex2i(x2, y2);
    }


    @Override
    public void init(GLAutoDrawable drawable) {

        GL2 gl = drawable.getGL().getGL2();

        gl.glClearColor(0, 0, 0, 0);
        gl.glClearDepth(1.0f);
        gl.glMatrixMode(GL2.GL_MATRIX_MODE);
        gl.glLoadIdentity();

		//para linhas:
		//GLU glu = new GLU();
        //glu.gluOrtho2D(-100, 100, -100, 100); 

    }

    @Override
    public void reshape(GLAutoDrawable arg0, int arg1, int arg2, int arg3, int arg4) {
    }

    @Override
    public void dispose(GLAutoDrawable arg0) {
    }

    private static byte[] getMaskHalftone() {

        byte[] mask = new byte[128];
        byte m = (byte) 0xAA;
        for (int i = 0; i < 128; i++) {

            if (i % 4 == 0) {
                if (m == (byte) 0xAA) {
                    m = (byte) 0x55;
                } else {
                    m = (byte) 0xAA;
                }
            }

            mask[i] = m;
        }

        return mask;
    }

    private static byte[] getMaskFly() {

        byte mask0[] = new byte[128];

        mask0[0] = (byte) 0x00;
        mask0[1] = (byte) 0x00;
        mask0[2] = (byte) 0x00;
        mask0[3] = (byte) 0x00;
        mask0[4] = (byte) 0x00;
        mask0[5] = (byte) 0x00;
        mask0[6] = (byte) 0x00;
        mask0[7] = (byte) 0x00;

        mask0[8] = (byte) 0x03;
        mask0[9] = (byte) 0x80;
        mask0[10] = (byte) 0x01;
        mask0[11] = (byte) 0xC0;
        mask0[12] = (byte) 0x06;
        mask0[13] = (byte) 0xC0;
        mask0[14] = (byte) 0x03;
        mask0[15] = (byte) 0x60;

        mask0[16] = (byte) 0x04;
        mask0[17] = (byte) 0x60;
        mask0[18] = (byte) 0x06;
        mask0[19] = (byte) 0x20;
        mask0[20] = (byte) 0x04;
        mask0[21] = (byte) 0x30;
        mask0[22] = (byte) 0x0C;
        mask0[23] = (byte) 0x20;

        mask0[24] = (byte) 0x04;
        mask0[25] = (byte) 0x18;
        mask0[26] = (byte) 0x18;
        mask0[27] = (byte) 0x20;
        mask0[28] = (byte) 0x04;
        mask0[29] = (byte) 0x0C;
        mask0[30] = (byte) 0x30;
        mask0[31] = (byte) 0x20;

        mask0[32] = (byte) 0x04;
        mask0[33] = (byte) 0x06;
        mask0[34] = (byte) 0x60;
        mask0[35] = (byte) 0x20;
        mask0[36] = (byte) 0x44;
        mask0[37] = (byte) 0x03;
        mask0[38] = (byte) 0xC0;
        mask0[39] = (byte) 0x22;

        mask0[40] = (byte) 0x44;
        mask0[41] = (byte) 0x01;
        mask0[42] = (byte) 0x80;
        mask0[43] = (byte) 0x22;
        mask0[44] = (byte) 0x44;
        mask0[45] = (byte) 0x01;
        mask0[46] = (byte) 0x80;
        mask0[47] = (byte) 0x22;

        mask0[48] = (byte) 0x44;
        mask0[49] = (byte) 0x01;
        mask0[50] = (byte) 0x80;
        mask0[51] = (byte) 0x22;
        mask0[52] = (byte) 0x44;
        mask0[53] = (byte) 0x01;
        mask0[54] = (byte) 0x80;
        mask0[55] = (byte) 0x22;

        mask0[56] = (byte) 0x44;
        mask0[57] = (byte) 0x01;
        mask0[58] = (byte) 0x80;
        mask0[59] = (byte) 0x22;
        mask0[60] = (byte) 0x44;
        mask0[61] = (byte) 0x01;
        mask0[62] = (byte) 0x80;
        mask0[63] = (byte) 0x22;

        mask0[64] = (byte) 0x66;
        mask0[65] = (byte) 0x01;
        mask0[66] = (byte) 0x80;
        mask0[67] = (byte) 0x66;
        mask0[68] = (byte) 0x33;
        mask0[69] = (byte) 0x01;
        mask0[70] = (byte) 0x80;
        mask0[71] = (byte) 0xCC;

        mask0[72] = (byte) 0x19;
        mask0[73] = (byte) 0x81;
        mask0[74] = (byte) 0x81;
        mask0[75] = (byte) 0x98;
        mask0[76] = (byte) 0x0C;
        mask0[77] = (byte) 0xC1;
        mask0[78] = (byte) 0x83;
        mask0[79] = (byte) 0x30;

        mask0[80] = (byte) 0x07;
        mask0[81] = (byte) 0xe1;
        mask0[82] = (byte) 0x87;
        mask0[83] = (byte) 0xe0;
        mask0[84] = (byte) 0x03;
        mask0[85] = (byte) 0x3f;
        mask0[86] = (byte) 0xfc;
        mask0[87] = (byte) 0xc0;

        mask0[88] = (byte) 0x03;
        mask0[89] = (byte) 0x31;
        mask0[90] = (byte) 0x8C;
        mask0[91] = (byte) 0xC0;
        mask0[92] = (byte) 0x03;
        mask0[93] = (byte) 0x33;
        mask0[94] = (byte) 0xCC;
        mask0[95] = (byte) 0xC0;

        mask0[96] = (byte) 0x06;
        mask0[97] = (byte) 0x64;
        mask0[98] = (byte) 0x26;
        mask0[99] = (byte) 0x60;
        mask0[100] = (byte) 0x0C;
        mask0[101] = (byte) 0xCC;
        mask0[102] = (byte) 0x33;
        mask0[103] = (byte) 0x30;

        mask0[104] = (byte) 0x18;
        mask0[105] = (byte) 0xCC;
        mask0[106] = (byte) 0x33;
        mask0[107] = (byte) 0x18;
        mask0[108] = (byte) 0x10;
        mask0[109] = (byte) 0xC4;
        mask0[110] = (byte) 0x23;
        mask0[111] = (byte) 0x08;

        mask0[112] = (byte) 0x10;
        mask0[113] = (byte) 0x63;
        mask0[114] = (byte) 0xC6;
        mask0[115] = (byte) 0x08;
        mask0[116] = (byte) 0x10;
        mask0[117] = (byte) 0x30;
        mask0[118] = (byte) 0x0C;
        mask0[119] = (byte) 0x08;

        mask0[120] = (byte) 0x10;
        mask0[121] = (byte) 0x18;
        mask0[122] = (byte) 0x18;
        mask0[123] = (byte) 0x08;
        mask0[124] = (byte) 0x10;
        mask0[125] = (byte) 0x00;
        mask0[126] = (byte) 0x00;
        mask0[127] = (byte) 0x08;

        return mask0;
    }

    public static void main(String[] args) {

        GLProfile profile = GLProfile.get(GLProfile.GL2);

        GLCapabilities caps = new GLCapabilities(profile);

        GLCanvas canvas = new GLCanvas(caps);
        canvas.setSize(900, 500);
        canvas.addGLEventListener(new Renderer());

        JFrame frame = new JFrame("Aplicacao JOGL");
        frame.getContentPane().add(canvas);
        frame.setSize(900, 500);
        frame.setBackground(Color.BLUE);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

}
